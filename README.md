# OTSDocker
## Version 0.9.1 Instructions

This is still under construction. Some parts of the image are not polished. Instructions will be given for podman, but docker can be substituted. This has been tested for host machines running linux of some form. 

### FIRST TIME EASY SETUP

Cactus must be installed and running on your system. 


Clone this directory outside of the container:
<pre>
git clone --single-branch --branch develop https://gitlab.cern.ch/otsdaq/otsdaq-cms-burninbox-data.git
</pre>

To set up the environment variables, edit this file otsdaq-cms-burninbox-data/CreatorSetup.sh. You can change which tool you use, docker or podman, and you can change what the name of the container will be. When you've decided, run:

<pre>
source otsdaq-cms-burninbox-data/CreatorSetup.sh
</pre>

Now, make a directory named OtsdaqData wherever you want the data stored. Then run:

<pre>
./otsdaq-cms-burninbox-data/Scripts/DockerCreator.sh PATH_TO_otsdaq-cms-burninbox-data PATH_TO_OtsdaqData
</pre>

Note, put the path to the directories, but don't include the directory names in the path. If these are both in your home directory, then it would simply be:

<pre>
./otsdaq-cms-burninbox-data/Scripts/DockerCreator.sh $HOME $HOME
</pre>

If this runs correctly, it should be set up!

### TMUX LAUNCH

To launch the burninbox and all of its related programs in a tmux application, run 



<pre>
source otsdaq-cms-burninbox-data/CreatorSetup.sh
./otsdaq-cms-burninbox-data/LaunchTmux.sh podman_BurninBox
</pre>

To stop it, do "Control + b" then "&" and then hit "y". Outside of the tmux application, use 
<pre>
podman stop podman_BurninBox
</pre>
to stop the container.
### MANUAL FIRST TIME SETUP 

Cactus must be installed and running on your system. 

First, pull the podman image (if running on docker remove `--storage-opt ignore_chown_errors=true`). This might take a while since the docker image is currently bloated and we are in the process of trimming the size: 

<pre>
podman --storage-opt ignore_chown_errors=true pull gitlab-registry.cern.ch/otsdaq/otsdocker/tracker_al9:remove_burninbox
</pre>

Now, clone this directory outside of the container:
<pre>
git clone --single-branch --branch develop https://gitlab.cern.ch/otsdaq/otsdaq-cms-burninbox-data.git
</pre>

Now run the image with these options:
<pre>
podman run --privileged --net host -p 2015:2015 -p 2020:2020 -p 2021:2021 -p 2022:2022 -p 2023:2023 --name $CONTAINER -h $HOSTNAME -v $PATH_TO_DIR/otsdaq-cms-burninbox-data:/otsdaq/otsdaq-cms-burninbox-data -v $PATH_TO_DIR/OtsdaqData:/otsdaq/OtsdaqData -i gitlab-registry.cern.ch/${ORIGIN}/otsdocker/$IMAGE:$TAG
</pre>
You'll need to fill out the correct paths to otsdaq-cms-burninbox-data and OtsdaqData. You'll also need to choose an container name for $CONTAINER. Finally, you'll need to pick the $ORIGIN, $IMAGE, and $TAG. Currently, those values should be $ORIGIN=avendras, $IMAGE=tracker_al9, $TAG=main.

This command will place you inside the running container. Now, run the following script:

<pre>
source otsdaq-cms-burninbox-data/SetupFiles/ContainerFirstTimeSetup.sh
</pre>


To run the Ph2_ACF porperly you need to go into the xml file for module and change the ip address listed to the ip address of the host machine. This will be in this file (unless you need to use a different xml for whatever reason):

<pre>
vi /otsdaq/otsdaq-cms-burninbox-data/Ph2_ACF/settings/2S_4Modules_Burnin.xml





#Edit the following line changing localhost with the IP address of your HOST MACHINE and 192.168.0.20 with your target FC7
connection id="board" uri="chtcp-2.0://localhost:10203?target=192.168.0.20:50001" address_table="file://${PH2ACF_BASE_DIR}/settings/address_tables/uDTC_OT_address_table.xml"
</pre>

In line 5, set the target equal to the ip address of your HOST MACHINE, leaving that port 50001:

##Running OTSDAQ
Inside the image source the BurninBoxSetup.sh script, then run the ots wizard only the very first time after installation. NOTE: BurninBoxSetup.sh MUST be source from the /otsdaq directory, not inside the /otsdaq/otsdaq-cms-burninbox-data/SetupFiles/ directory.
<pre>
source otsdaq-cms-burninbox-data/SetupFiles/BurninBoxSetup.sh
ots -w #THIS MUST BE RUN ONLY THE VERY FIRST TIME, AFTER THAT ONLY ots
#WAIT UNTIL IT IS DONE
</pre>

When you source the BurninBoxSetup.sh script several errors related to kinit tickets will throw at the end. You can ignore this. It's there for legacy support reasons and will be removed soon.


Now the container is fully setup. Exit the container using ```exit```.

### MANUAL RUNNING SETUP

We now need to open three tabs in the docker container to run ots, the RunController, and the BurninBoxController. Open three terminals of the host machine. First, we need to find the name of the running podman image:
<pre>
podman ps -a
</pre>

This will list all of the containers you have made on this machine as this user. Find the container that you just made in the First Time Setup section. We can find the name under the column "NAMES", but often the column are squished and it's hard to read, so look for something of the form "adjective_noun". To start the now properly configured container, do this command:
<pre>
podman start NAME_OF_CONTAINER
</pre>

 Now, in each of the three tabs of the host machine run these:

<pre>
podman exec -i -t NAME_OF_CONTAINER bash
source otsdaq-cms-burninbox-data/SetupFiles/BurninBoxSetup.sh
ots
</pre>

In the next terminal
<pre>
podman exec -i -t NAME_OF_CONTAINER bash
source otsdaq-cms-burninbox-data/SetupFiles/BurninBoxSetup.sh
BurninBoxController
</pre>

In another terminal
<pre>
podman exec -i -t NAME_OF_CONTAINER bash
source otsdaq-cms-burninbox-data/SetupFiles/BurninBoxSetup.sh
RunController
</pre>

Now you should have three tabs open, one running ots, one with BurninBoxController, and one with the RunController. You can open ots in a browser and run the burnin box controls like normal.


To stop the container, use ```exit``` to get out of them, and then run ```podman stop NAME_OF_CONTAINER```


Direct any questions to jlawless@fnal.gov, or on the CMS Mattermost as John Steven Lawless.




